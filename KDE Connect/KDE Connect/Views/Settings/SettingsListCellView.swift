//
//  SettingsListCellView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-06-17.
//

//import SwiftUI
//
//struct SettingsListCellView: View {
//    let entryTitle: String
//    @State var entryDescription: String
//
//    var body: some View {
//        Button(action: {
//
//        }, label: {
//            HStack {
//                Text(entryTitle)
//                    .padding(.leading, 15)
//                Spacer()
//                Text(entryDescription)
//                    .padding(.trailing, 15)
//            }
//        })
//    }
//}

//struct SettingsListCellView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingsListCellView(entryTitle: "Device Name", entryDescription: "iPhone 7")
//    }
//}
